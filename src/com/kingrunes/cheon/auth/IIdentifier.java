package com.kingrunes.cheon.auth;

public interface IIdentifier
{
	public String getIdentity(String nick);
}