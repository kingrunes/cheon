package com.kingrunes.cheon;

import com.kingrunes.cheon.api.ICommInterface;
import com.kingrunes.cheon.auth.IAuthorizer;
import com.kingrunes.cheon.auth.IIdentifier;
import com.kingrunes.cheon.configuration.Configuration;

public class CheonBot
{
	private ICommInterface commInterface;
	private IIdentifier identifier;
	
	private IAuthorizer authorizer;
	
	private Configuration configuration;
	
	public CheonBot(Configuration configuration)
	{
		this.configuration = configuration;
	}
}