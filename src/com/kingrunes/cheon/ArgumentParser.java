package com.kingrunes.cheon;

import java.util.HashMap;

public class ArgumentParser
{
	public static ArgumentParser parse(String[] args, String prefix)
	{
		HashMap<String, String> argMap = new HashMap<String, String>();
		String value;
		int j;
		for (int i=0; i<args.length; i++)
		{
			j = i+1;
			if (j >= args.length)
				value = "";
			else
				value = args[j];
			j = i;
			if (value.startsWith(prefix))
				value = "";
			else
				j++;
			argMap.put(args[i], value);
			i = j;
		}
		return new ArgumentParser(argMap);
	}
	
	private HashMap<String, String> argMap;
	
	private ArgumentParser()
	{
		this(new HashMap<String, String>());
	}
	
	private ArgumentParser(HashMap<String, String> argMap)
	{
		this.argMap = argMap;
	}
	
	public String getArgument(String key, String def)
	{
		String val = argMap.get(key);
		if (val == null)
			return def;
		return val;
	}
	
	public String getArgument(String longKey, String shortKey, String def)
	{
		return getArgument(longKey, getArgument(shortKey, def));
	}

	public HashMap<String, String> getMap()
	{
		return argMap;
	}

	public static int[] buildIntArray(String[] stra)
	{
		int[] inta = new int[stra.length];
		String str;
		for (int i=0; i<inta.length; i++)
		{
			str = stra[i];
			if (str.trim().length() > 0)
				inta[i] = Integer.parseInt(str);
		}
		return inta;
	}
}