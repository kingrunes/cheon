package com.kingrunes.cheon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.kingrunes.cheon.configuration.Configuration;

public class Cheon
{
	private List<CheonBot> bots;
	
	public Cheon()
	{
		this.bots = Collections.synchronizedList(new ArrayList<CheonBot>());
	}

	public void start()
	{
		
	}

	public void createBot(Configuration configuration)
	{
		bots.add(new CheonBot(configuration));
	}
}