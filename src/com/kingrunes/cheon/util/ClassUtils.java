package com.kingrunes.cheon.util;

import java.lang.reflect.InvocationTargetException;

public class ClassUtils
{
	@SuppressWarnings("unchecked")
	public static <T> T instantiate(Class<?> clazz) throws InstantiationException, IllegalAccessException
	{
		return (T) clazz.newInstance();
	}
	
	public static Object invokeMethod(Class<?> clazz, String name, Object obj, Class<?>[] parameterTypes, Object[] args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException 
	{
		return clazz.getMethod(name, parameterTypes).invoke(obj, args);
	}
	
	public static Object invokeStaticMethod(Class<?> clazz, String name, Class<?>[] parameterTypes, Object[] args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException 
	{
		return invokeMethod(clazz, name, null, parameterTypes, args);
	}
}