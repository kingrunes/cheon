package com.kingrunes.cheon;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kingrunes.cheon.configuration.Configuration;
import com.kingrunes.cheon.util.ClassUtils;
import com.thoughtworks.xstream.XStream;

public class Program
{
	private static Logger logger;
	private static Cheon instance;
	
	public static void main(String[] args)
	{
		// Start argument parsing
		ArgumentParser argumentParser = ArgumentParser.parse(args, "-");
		// End

		// Start help test
		if (argumentParser.getArgument("-h", "--help", null) != null)
		{
			printHelp();
			System.exit(0);
		}
		// End
		
		// Start logger setup
		String loggerFactoryClassStr = argumentParser.getArgument("-l", "--logger-factory", null);
		if (loggerFactoryClassStr == null)
			logger = LoggerFactory.getLogger("Cheon");
		else
		{
			Class<?> clazz; 
			try
			{
				clazz = Class.forName(loggerFactoryClassStr);
			}
			catch (ClassNotFoundException cnfe)
			{
				System.err.println("Unable to find LoggerFactory class: " + loggerFactoryClassStr);
				cnfe.printStackTrace();
				System.exit(1);
				return;
			}
			if (ILoggerFactory.class.isAssignableFrom(clazz))
			{
				Object loggerObj;
				try
				{
					loggerObj = ClassUtils.invokeStaticMethod(clazz, "getLogger", new Class<?>[]{ String.class }, new Object[]{ "Cheon" });
				}
				catch
				(
					IllegalAccessException | IllegalArgumentException |
					InvocationTargetException | NoSuchMethodException |
					SecurityException e
				)
				{
					e.printStackTrace();
					System.exit(1);
					return;
				}
				if (loggerObj instanceof Logger)
				{
					logger = (Logger) loggerObj;
				}
				else
				{
					System.err.println("Logger object returned from " + clazz.getCanonicalName() + " does not implement " + Logger.class.getCanonicalName());
					System.exit(1);
					return;
				}
			}
			else
			{
				System.err.println("LoggerFactory class " + loggerFactoryClassStr + " does not implement " + ILoggerFactory.class.getCanonicalName());
				System.exit(1);
				return;
			}
		}
		// End
		
		// Start configuration setup
		logger.debug("Fetching configuration file");
		File configurationFile = new File(argumentParser.getArgument("--config", "-c", "config.xml"));
		XStream xStream = new XStream();
		Configuration configuration = null;
		if (configurationFile.exists())
		{
			logger.info("Loading configuration from " + configurationFile.getAbsolutePath());
			Object obj = xStream.fromXML(configurationFile);
			if (obj instanceof Configuration)
			{
				configuration = (Configuration)obj;
			}
			else
			{
				logger.error("Configuration is not instance of " + Configuration.class.getCanonicalName());
				System.exit(1);
			}
		}
		else
		{
			try
			{
				configuration = new Configuration();
				logger.debug("Opening FileOutputStream");
				FileOutputStream fos = new FileOutputStream(configurationFile);
				logger.info("Writing configuration file to " + configurationFile.getAbsolutePath());
				xStream.toXML(configuration, fos);
				fos.close();
			}
			catch (IOException ioe)
			{
				logger.error("Unable to load configuration: " + configurationFile.getAbsolutePath(), ioe);
			}
		}
		if (configuration == null)
		{
			logger.debug("Configuration is null, will exit!");
			System.exit(1);
		}
		// End
		
		// Start instantiation and initiation
		logger.debug("Instantiating Cheon");
		instance = new Cheon();
		instance.createBot(configuration);
		logger.info("Starting Cheon instance");
		instance.start();
		//
		
		// DEBUG
		logger.debug("Execution complete!");
		//
	}
	
	private static void printHelp()
	{
		System.out.print(
				"-h <=> --help      => Show this\n"
				+
				"-l <=> --logger-factory => Use custom logger factory class\n"
				+
				"-c <=> --config    => Use custom configuration file (default: config.xml)\n"
				);
	}
	
	public static Cheon instance()
	{
		return instance;
	}
	
	public static Logger getLogger()
	{
		return logger;
	}
}