package com.kingrunes.cheon.irc.pbx;

import org.pircbotx.Configuration;
import org.pircbotx.PircBotX;

import com.kingrunes.cheon.api.ICommInterface;

public class CheonPBX extends PircBotX implements ICommInterface
{
	public CheonPBX(Configuration<CheonPBX> configuration)
	{
		super(configuration);
	}
}